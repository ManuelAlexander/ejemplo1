
package contarpalabras;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.StringTokenizer;

public class ContarPalabras {
 
    public static void main(String...alf) throws FileNotFoundException, IOException
    {//*********************************
        String str ="";
     FileReader entrada = null;
     
      String registro="";
      try{
            entrada = new FileReader("trabalenguas.txt");
            BufferedReader leer = new BufferedReader(entrada);                      
           while((registro=leer.readLine()) != null){
               str=str+registro;
               
     }
             }
        finally
        {   if(entrada!=null)
            {entrada.close();}}
     //*********************************
        LinkedList <Palabra> lista = new <Palabra> LinkedList(); //para que los objetos sean de ese tipo
        
        String temporal="";
        //String str = "pablito,clavo,un,clavito sobre.la calva"+ " de un calvito;pablito clavo.un, clavito";
        StringTokenizer stC =  new StringTokenizer(str,",");//para quitar (,)
        while(stC.hasMoreTokens())
        {temporal=temporal+stC.nextToken()+" ";
        }
        StringTokenizer stP =  new StringTokenizer(temporal,".");//para quitar (,)
        temporal="";
        while(stP.hasMoreTokens())
        {temporal=temporal+stP.nextToken()+" ";
        }
        StringTokenizer stPC =  new StringTokenizer(temporal,";");//para quitar (,)
        temporal="";
        while(stPC.hasMoreTokens())
        {temporal=temporal+stPC.nextToken()+" ";
        }
        StringTokenizer st =  new StringTokenizer(temporal);

        String tmp="";
        boolean esta = false;
        Palabra palabra = null;
        
        int n=0;
        
        while(st.hasMoreTokens())
        {
            esta=false;
            tmp = st.nextToken();
            
              if(lista.isEmpty())
              {
                palabra = new Palabra(tmp,1);
                lista.add(palabra);
              }
            
            else
            {
                n = lista.size();
                
                for(int i=0; i<n; i++)
                 {
                    palabra = lista.get(i);
                    //System.out.println(palabra.getOalabra());
                       
                       if(palabra.getOalabra().equals(tmp))
                       {
                           palabra.setFrecuencia(palabra.getFrecuencia()+1);
                           
                           lista.set(i, palabra);
                           esta = true;
                       }
                 }
                
                  if(!esta)
                  {
                    palabra = new Palabra(tmp,1);
                    lista.add(palabra);
                  }
            }
              
        }  
        FileWriter salida = null;
     
        try{
            salida = new FileWriter("Resultados.txt");
        String a=lista.toString();
          System.out.println(a);
          
          salida.write(a);
        
        }finally
        {
            if(salida!=null)
            {
               salida.close();
            }
         }
        
          //System.out.println(lista.contains("caperucita"));
         // palabra = lista.get(0);
         // System.out.println("BUSQUEDA POR INDICE" + palabra.getOalabra());
          
         
         

        
    }
    
}
